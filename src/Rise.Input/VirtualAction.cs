﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Rise.Input
{
    public class VirtualAction : VirtualData<bool>
    {
        /// <summary>
        /// Action is 
        /// </summary>
        public bool IsPerformed { get => Value; }

        /// <summary>
        /// Will called when action is starting perform
        /// </summary>
        public event Action OnStart = delegate { };

        /// <summary>
        /// Will called when action is ended
        /// </summary>
        public event Action OnEnd   = delegate { };

        public VirtualAction(string name) : this(name, false)
        {
        }

        public VirtualAction(string name, bool value) : base(name, value)
        {
        }

        protected override void OnValueUpdated(bool previous, bool current)
        {
            base.OnValueUpdated(previous, current);

            if (previous == current) return;

            if (current)
            {
                OnStart.Invoke();
            }
            else
            {
                OnEnd.Invoke();
            }
        }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Rise.Input
{
    public class InputRouter : IInputRouter
    {
        private List<IInputRoute> m_Routes = new List<IInputRoute>();

        /// <summary>
        /// Adds new route to router.
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="route"></param>
        public void AddRoute<T>(T route) where T : class, IInputRoute
        {
            if (ContainsRoute<T>(route.Name))
            {
                throw new Exception($"Route with name '{route.Name}' and type '{typeof(T)}' already defined");
            }

            m_Routes.Add(route);
        }

        /// <summary>
        /// Return router by type and name.
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="routeName"></param>
        /// <returns></returns>
        public T GetRoute<T>(string routeName) where T : class, IInputRoute
        {
            if (string.IsNullOrEmpty(routeName))
            {
                throw new ArgumentNullException($"{nameof(routeName)} must not be null or empty.");
            }

            var matchType = typeof(T);
            return m_Routes.FirstOrDefault(r => routeName.Equals(r.Name) && r.GetType().Equals(matchType)) as T;
        }

        /// <summary>
        /// Return true if router contains the route.
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="route"></param>
        /// <returns></returns>
        public bool ContainsRoute<T>(T route) where T : IInputRoute
        {
            return m_Routes.Contains(route);
        }

        /// <summary>
        /// Return true if a router includes a route with the specified type and name.
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="routeName"></param>
        /// <returns></returns>
        public bool ContainsRoute<T>(string routeName) where T : class, IInputRoute
        {
            return GetRoute<T>(routeName) != null;
        }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Rise.Input.Internal
{
    public static class Utils
    {
        public static void ValidateOrThrowArgumentNullException<T>(T obj, string message)
        {
            if (obj == null)
            {
                throw new ArgumentNullException(message);
            }
        }

        public static void ValidateOrThrowArgumentNullException(IInputRouter router)
        {
            if (router == null)
            {
                throw new ArgumentNullException("Input router can not be a null");
            }
        }
    }
}

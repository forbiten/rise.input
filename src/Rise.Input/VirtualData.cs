﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Rise.Input
{
    public class VirtualData<T> : InputRoute
        where T : struct
    {
        /// <summary>
        /// Value update event.
        /// Will called after value changed.
        /// </summary>
        public event Action<T> OnUpdate = delegate { };
        
        /// <summary>
        /// Data.
        /// </summary>
        public T Value { get; private set; }
        
        public VirtualData(string name) : this(name, default(T))
        {
        }

        public VirtualData(string name, T value) : base(name)
        {
            Value = value;
        }

        /// <summary>
        /// Update value and call events.
        /// </summary>
        /// <param name="value"></param>
        public void Update(T value)
        {
            var previous = Value;
            Value = value;

            OnValueUpdated(previous, value);
            OnUpdate.Invoke(Value);
        }

        /// <summary>
        /// Will called after value was changed.
        /// </summary>
        /// <param name="value"></param>
        protected virtual void OnValueUpdated(T previous, T current)
        {
        }
    }
}

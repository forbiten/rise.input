﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using UnityEngine;

namespace Rise.Input
{
    public class VirtualAxis : VirtualData<Vector2>
    {
        public VirtualAxis(string name) : base(name)
        {
        }

        public VirtualAxis(string name, Vector2 value) : base(name, value)
        {
        }
    }
}

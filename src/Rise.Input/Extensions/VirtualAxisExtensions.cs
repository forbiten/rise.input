﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using UnityEngine;

namespace Rise.Input
{
    public static class VirtualAxisExtensions
    {
        public static VirtualAxis GetOrCreateVirtualAxis(this IInputRouter router, string routeName, Vector2 axis)
        {
            Internal.Utils.ValidateOrThrowArgumentNullException(router);
            return router.GetOrCreateRoute(routeName, _ => new VirtualAxis(routeName, axis));
        }

        public static VirtualAxis GetOrCreateVirtualAxis(this IInputRouter router, string routeName)
        {
            return GetOrCreateVirtualAxis(router, routeName, Vector2.zero);
        }

        public static VirtualAxis GetVirtualAxis(this IInputRouter router, string routeName)
        {
            Internal.Utils.ValidateOrThrowArgumentNullException(router);
            return router.GetRoute<VirtualAxis>(routeName);
        }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Rise.Input
{
    public static class InputRouterExtensions
    {
        public static TRoute GetOrCreateRoute<TRoute>(this IInputRouter router, string routeName, Func<IInputRouter, TRoute> builder)
            where TRoute : class, IInputRoute
        {
            if (router == null)
            {
                throw new ArgumentNullException(nameof(router));
            }

            var route = router.GetRoute<TRoute>(routeName);

            if (route == null)
            {
                route = builder.Invoke(router);
                router.AddRoute(route);
            }

            return route;
        }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Rise.Input
{
    public static class VirtualActionExtensions
    {
        public static VirtualAction GetOrCreateVirtualAction(this IInputRouter router, string routeName, bool state)
        {
            Internal.Utils.ValidateOrThrowArgumentNullException(router);
            return router.GetOrCreateRoute(routeName, _ => new VirtualAction(routeName, state));
        }

        public static VirtualAction GetOrCreateVirtualAction(this IInputRouter router, string routeName)
        {
            return GetOrCreateVirtualAction(router, routeName, false);
        }

        public static VirtualAction GetVirtualAction(this IInputRouter router, string routeName)
        {
            Internal.Utils.ValidateOrThrowArgumentNullException(router);
            return router.GetRoute<VirtualAction>(routeName);
        }
    }
}

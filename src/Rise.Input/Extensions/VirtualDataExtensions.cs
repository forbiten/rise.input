﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Rise.Input
{
    public static class VirtualDataExtensions
    {
        public static VirtualData<T> GetOrCreateVirtualData<T>(this IInputRouter router, string routeName, T value)
            where T : struct
        {
            Internal.Utils.ValidateOrThrowArgumentNullException(router);
            return router.GetOrCreateRoute(routeName, _ => new VirtualData<T>(routeName, value));
        }

        public static VirtualData<T> GetOrCreateVirtualData<T>(this IInputRouter router, string routeName)
            where T : struct
        {
            return GetOrCreateVirtualData(router, routeName, default(T));
        }

        public static VirtualData<T> GetVirtualData<T>(this IInputRouter router, string routeName)
            where T : struct
        {
            Internal.Utils.ValidateOrThrowArgumentNullException(router);
            return router.GetRoute<VirtualData<T>>(routeName);
        }

        public static void AddVirtualData<T>(this IInputRouter router, string routeName, T value)
            where T : struct
        {
            Internal.Utils.ValidateOrThrowArgumentNullException(router);
            router.AddRoute(new VirtualData<T>(routeName, value));
        }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Rise.Input
{
    /// <summary>
    /// Input route abstraction.
    /// </summary>
    public abstract class InputRoute : IInputRoute
    {
        /// <summary>
        /// Route name.
        /// </summary>
        public string Name { get; private set; }

        public InputRoute(string name)
        {
            Name = name;
        }
    }
}

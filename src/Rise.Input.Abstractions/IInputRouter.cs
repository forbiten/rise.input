﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Rise.Input
{
    public interface IInputRouter
    {
        void AddRoute<T>(T route) where T : class, IInputRoute;
        T GetRoute<T>(string routeName) where T : class, IInputRoute;
        bool ContainsRoute<T>(T route) where T : IInputRoute;
        bool ContainsRoute<T>(string routeName) where T : class, IInputRoute;
    }
}

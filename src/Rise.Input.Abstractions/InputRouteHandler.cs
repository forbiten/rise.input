﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Rise.Input
{
    public abstract class InputRouteHandler<TRoute>
        where TRoute : IInputRoute
    {
        public TRoute Route { get; protected set; }

        public InputRouteHandler(TRoute route)
        {
            Route = route;
        }
    }
}
